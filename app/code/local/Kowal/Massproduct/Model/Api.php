<?php

require_once 'Kowal/Massproduct/lib/MagentoQueries.php';

class Kowal_Massproduct_Model_Api extends Mage_Api_Model_Resource_Abstract
{
    /**
     * @param $data
     * @return bool
     */
    public function Update($data)
    {
        $mag = new MagentoQueries();

        $message = '';
        $count = 1;
        foreach ($data as $_data) {
            if ($mag->_checkIfSkuExists($_data->product_id, 'entity_id')) {
                try {
//                    $mag->_updateStocks($_data);
                    $message = $mag->_updateAttribute($_data->product_id, $_data->attribute_id, $_data->attribute_value, $_data->attribute_key);
                    if (!$message) file_put_contents("__webservice_attribute_update_" . date('Hms') . ".txt", print_r($message, true));
                    return (bool)$message;
                } catch (Exception $e) {
                    $message .= $count . '> Error:: while Upating  Qty (' . $_data->attribute_value . ') of Id (' . $_data->product_id . ') => ' . $e->getMessage() . '<br />';
                }
            } else {
                $message .= $count . '> Error:: Product with Id (' . $_data->product_id . ') does\'t exist.<br />';
            }
            $count++;
        }
        file_put_contents("__webservice_attribute_update_" . date('Hms') . ".txt", print_r($message, true));
        return false;
    }
}