<?php

/**
 * Class MagentoQueries
 */
class MagentoQueries
{

    /***************** UTILITY FUNCTIONS ********************/

    function _getConnection($type = 'core_read')
    {
        return Mage::getSingleton('core/resource')->getConnection($type);
    }

    function _getTableName($tableName)
    {
        return Mage::getSingleton('core/resource')->getTableName($tableName);
    }

    function _getAttributeId($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT attribute_id 
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId();
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }

    function _getAttributeType($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT backend_type
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId();
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }

    function _getEntityTypeId($entity_type_code = 'catalog_product')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne($sql, array($entity_type_code));
    }

    function _checkIfSkuExists($sku_or_entity_id, $field_name = 'sku')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT COUNT(*) AS count_no FROM " . $this->_getTableName('catalog_product_entity') . "	WHERE {$field_name} = ?";
        $count = $connection->fetchOne($sql, array($sku_or_entity_id));
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne($sql, array($sku));
    }

    function _updateStocks($data)
    {
        try {
            $connection = $this->_getConnection('core_write');
//            $connection->beginTransaction();

            $sku = $data[0];
            $newQty = $data[1];
            $productId = $this->_getIdFromSku($sku);
//            $attributeId = $this->_getAttributeId();

            $sql = "UPDATE " . $this->_getTableName('cataloginventory_stock_item') . " csi,
					   " . $this->_getTableName('cataloginventory_stock_status') . " css
	                   SET
					   csi.qty = ?,
					   csi.is_in_stock = ?,
	                   css.qty = ?,
					   css.stock_status = ?
					   WHERE
					   csi.product_id = ?
			           AND csi.product_id = css.product_id";
            $isInStock = $newQty > 0 ? 1 : 0;
            $stockStatus = $newQty > 0 ? 1 : 0;
            $connection->query($sql, array($newQty, $isInStock, $newQty, $stockStatus, $productId));

//            $connection->commit();
            return true;

        } catch (Exception $e) {
//            $connection->rollBack();
            return ['error' => $e->getMessage()];
        }
    }

    function _updatePrices($data)
    {
        try {
            $connection = $this->_getConnection('core_write');
//            $connection->beginTransaction();

            $sku = $data[0];
            $newPrice = $data[1];
            $productId = $this->_getIdFromSku($sku);
            $attributeId = $this->_getAttributeId();

            $sql = "UPDATE " . $this->_getTableName('catalog_product_entity_decimal') . " cped
				SET  cped.value = ?
			WHERE  cped.attribute_id = ?
			AND cped.entity_id = ?";
            $connection->query($sql, array($newPrice, $attributeId, $productId));
//            $connection->commit();

        } catch (Exception $e) {
//            $connection->rollBack();
            return ['error' => $e->getMessage()];
        }
    }

    function _updateAttribute($productId, $attributeId, $attributeValue, $attributeKey)
    {
        try {

            $attribute_type = $this->_getAttributeType($attributeKey);

            if ($attribute_type == 'static') return false;

            $connection = $this->_getConnection('core_write');
//            $connection->beginTransaction();

            $sql = "UPDATE " . $this->_getTableName('catalog_product_entity_' . $attribute_type) . " cped
				SET  cped.value = ?
			WHERE  cped.attribute_id = ?
			AND cped.entity_id = ?";
            $connection->query($sql, array($attributeValue, $attributeId, $productId));
//            $connection->commit();

            return true;

        } catch (Exception $e) {
//            $connection->rollBack();
            return $e->getMessage();
        }
    }

    /***************** UTILITY FUNCTIONS ********************/

}