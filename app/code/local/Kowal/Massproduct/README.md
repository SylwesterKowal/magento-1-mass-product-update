# magento 1 mass product update

Demo in php 
SOAP

`$client = new SoapClient('http://yourhost/api/soap/?wsdl');`

`$session = $client->login('******', '******');`

`$date = $client->call($session, 'massproduct_api.update');`
											
 
 XML-RPC 
 
`$client = new Zend_XmlRpc_Client('http://yourhost/api/xmlrpc/');`

`$session = $client->call('login', array('******', '******'));`

`$date=$client->call('call', array($session, 'massproduct_api.update'));`